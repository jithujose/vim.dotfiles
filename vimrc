call pathogen#incubate()
call pathogen#helptags()
 filetype off
 syntax on
 filetype plugin indent on

" Start nerdtree on launch
" autocmd vimenter * NERDTree 
set guifont=Consolas:h11
colorscheme base16-eighties
:set guioptions-=m  "remove menu bar
:set guioptions-=T  "remove toolbar
:set guioptions-=r  "remove right-hand scroll bar
:set guioptions-=L  "remove left-hand scroll bar
set nu
:set tabstop=4
:set shiftwidth=4
:set expandtab

set runtimepath^=~/.vim/bundle/ctrlp.vim

"vim-airline
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

:set lines=35 columns=150
set tags=./tags,tags;$HOME
set omnifunc=syntaxcomplete#Complete
